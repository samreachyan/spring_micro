#!/bin/bash

echo "Starting build discovery service..."
cd discovery
mvn clean package
cd ..
clear
echo "Finished build discovery service."

echo "Starting config server service..."
cd configServer
mvn clean package
cd ..
clear
echo "Finished build config service."


echo "Starting build gateway service..."
cd gateway
mvn clean package
cd ..
clear
echo "Finished build gateway service."


echo "Starting build booking service..."
cd booking
mvn clean package
cd ..
clear
echo "Finished build booking service."


echo "Starting build accounting service..."
cd accounting
mvn clean package
cd ..
clear
echo "Finished build accounting service."